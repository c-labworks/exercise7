﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Xml;

namespace wfaPrimitive
{
    public class Canvas : Control
    {
        private int? m_iCurItem = 0;

        private int? mouseX = null;
        private int? mouseY = null;


        private Pen m_Pen = new Pen(Color.Red, 1);

        public List<BasePrimitive> Primitives { get; set; } = new List<BasePrimitive>();

        public void Init(XmlNode node)
        {
            BasePrimitive p;

            for (int i = 0; i < node.ChildNodes.Count; i++)
            {
                XmlNode child = node.ChildNodes[i];

                if (child.Name != "primitive")
                    break;

                XmlAttribute attr = child.Attributes["type"];
                if (attr == null)
                    continue;
                switch (attr.Value)
                {
                    case "FilledRectangle":
                        p = new FilledRectangle();
                        break;
                    case "Ellipse":
                        p = new Ellipse();
                        break;
                    case "Line":
                        p = new Line();
                        break;
                    case "Triangle":
                        p = new Triangle();
                        break;
                    default:
                        continue;
                }

                p.Init(child);
                Primitives.Add(p);
            }

            Refresh();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (m_iCurItem == null || mouseX == null || mouseY == null) return;
            var xDelta = e.X - mouseX;
            var yDelta = e.Y - mouseY;
            Primitives[m_iCurItem.Value].move(xDelta.Value, yDelta.Value);
            Refresh();
            m_iCurItem = null;

        }
        
        

        protected override void OnMouseDown(MouseEventArgs e)
        {
            mouseX = e.X;
            mouseY = e.Y;
            for (int i = 0; i < Primitives.Count; i++)
            {
                if (Primitives[i].DetectCollision(e.Location))
                {
                    m_iCurItem = i;
                    Console.WriteLine(Primitives[i].ToString());

                }
            }        
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (Primitives == null)
                return;

            BasePrimitive p = null;
            SolidBrush br;
            foreach (var primitive in Primitives)
            {
                p = primitive;
                switch (p)
                {
                    case FilledRectangle rectangle:
                    {
                        br = new SolidBrush(rectangle.FillColor);
                        g.FillRectangle(br, rectangle.Bounds);
                        break;
                    }
                    case Ellipse ellipse:
                        g.DrawEllipse(m_Pen, ellipse.Bounds);
                        break;
                    case Line line:
                        g.DrawLine(m_Pen, line.x1, line.y1, line.x2, line.y2);
                        break;
                    case Triangle triangle:
                        br = new SolidBrush(triangle.FillColor);
                        g.FillPolygon(br, triangle.pointArr, FillMode.Winding);
                        break;
                }
            }
        }

    }
}
