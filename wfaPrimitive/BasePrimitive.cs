﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace wfaPrimitive
{
    public abstract class BasePrimitive : IPrimitive
    {
        public int left { get; set; }
        public int top { get; set; }
        public int width { get; set; }
        public int height { get; set; }

        public Rectangle Bounds
        {
            get { return new Rectangle(left, top, width, height); }
        }

        public abstract bool DetectCollision(Point p);

        public abstract void move(int deltaX, int deltaY);

        public virtual void Init(XmlNode node)
        {
            XmlAttribute attr = node.Attributes["left"];
            if (attr != null)
                left = int.Parse(attr.Value);

            attr = node.Attributes["top"];
            if (attr != null)
                top = int.Parse(attr.Value);

            attr = node.Attributes["width"];
            if (attr != null)
                width = int.Parse(attr.Value);

            attr = node.Attributes["height"];
            if (attr != null)
                height = int.Parse(attr.Value);
        }
    }
}
