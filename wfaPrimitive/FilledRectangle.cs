﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace wfaPrimitive
{
    public class FilledRectangle : BasePrimitive
    {
        public FilledRectangle() { }
        public FilledRectangle(int left, int top, int width, int height, Color fillColor)
        {
            this.left = left;
            this.top = top;
            this.width = width;
            this.height = height;

            FillColor = fillColor;
        }

        public override bool DetectCollision(Point p)
        {
            if (Bounds.Contains(p))
                return true;

            return false;
        }

        public override void move(int deltaX, int deltaY)
        {
            left += deltaX;
            top += deltaY;        
        }

        public Color FillColor { get; set; }

        public override void Init(XmlNode node)
        {
            base.Init(node);

            XmlAttribute attr = node.Attributes["color"];
            if (attr == null)
                return;

            switch (attr.Value)
            {
                case "red":
                    FillColor = Color.Red;
                    break;
                case "green":
                    FillColor = Color.Green;
                    break;
                case "blue":
                    FillColor = Color.Blue;
                    break;
            }
        }
    }
}
