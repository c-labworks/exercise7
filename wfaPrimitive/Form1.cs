﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace wfaPrimitive
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            canvas1.Primitives.Add(new FilledRectangle(10, 10, 50, 100, Color.Red));
            canvas1.Primitives.Add(new FilledRectangle(100, 10, 100, 120, Color.Green));
            canvas1.Primitives.Add(new FilledRectangle(10, 100, 70, 50, Color.Blue));
            canvas1.Primitives.Add(new Ellipse(100, 100, 500, 100));
            canvas1.Primitives.Add(new Line(100, 100, 175, 175));
            canvas1.Primitives.Add(new Triangle(new Point(200, 200), new Point(320, 200), new Point(260, 260), Color.Blue));

        }

        private void экспортXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateNode("element", "root", "");
            doc.AppendChild(root);

            if (canvas1 == null)
                return;

            XmlNode canvas = doc.CreateNode("element", "canvas", "");
            root.AppendChild(canvas);

            XmlNode primitive = null;
            foreach (var pr in canvas1.Primitives)
            {
                BasePrimitive bp = pr;
                XmlAttribute attr;

                primitive = doc.CreateNode("element", "primitive", "");

                if (bp is Triangle triangle)
                {
                    attr = doc.CreateAttribute("x1");
                    attr.Value = triangle.p1.X.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("y1");
                    attr.Value = triangle.p1.Y.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("x2");
                    attr.Value = triangle.p2.X.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("y2");
                    attr.Value = triangle.p2.Y.ToString();
                    primitive.Attributes.Append(attr);
                    
                    attr = doc.CreateAttribute("x3");
                    attr.Value = triangle.p3.X.ToString();
                    primitive.Attributes.Append(attr);
                    
                    attr = doc.CreateAttribute("y3");
                    attr.Value = triangle.p3.Y.ToString();
                    primitive.Attributes.Append(attr);
                    
                    attr = doc.CreateAttribute("type");
                    attr.Value = "Triangle";
                    primitive.Attributes.Append(attr);
                    
                    attr = doc.CreateAttribute("color");
                    if (triangle.FillColor == Color.Red)
                        attr.Value = "red";
                    else if (triangle.FillColor == Color.Green)
                        attr.Value = "green";
                    else if (triangle.FillColor == Color.Blue)
                        attr.Value = "blue";
                    primitive.Attributes.Append(attr);

                }
                else if (bp is Line line)
                {
                    attr = doc.CreateAttribute("x1");
                    attr.Value = line.x1.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("y1");
                    attr.Value = line.y1.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("x2");
                    attr.Value = line.x2.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("y2");
                    attr.Value = line.y2.ToString();
                    primitive.Attributes.Append(attr);
                    
                    attr = doc.CreateAttribute("type");
                    attr.Value = "Line";
                    primitive.Attributes.Append(attr);
                }
                else
                {
                    attr = doc.CreateAttribute("left");
                    attr.Value = bp.left.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("top");
                    attr.Value = bp.top.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("width");
                    attr.Value = bp.width.ToString();
                    primitive.Attributes.Append(attr);

                    attr = doc.CreateAttribute("height");
                    attr.Value = bp.height.ToString();
                    primitive.Attributes.Append(attr);
                }
                
                attr = doc.CreateAttribute("type");
                if (pr is FilledRectangle)
                {
                    attr.Value = "FilledRectangle";
                    primitive.Attributes.Append(attr);

                    FilledRectangle r = pr as FilledRectangle;
                    attr = doc.CreateAttribute("color");
                    if (r.FillColor == Color.Red)
                        attr.Value = "red";
                    else if (r.FillColor == Color.Green)
                        attr.Value = "green";
                    else if (r.FillColor == Color.Blue)
                        attr.Value = "blue";

                    primitive.Attributes.Append(attr);
                }
                else if (pr is Ellipse)
                {
                    attr.Value = "Ellipse";
                    primitive.Attributes.Append(attr);
                }

                canvas.AppendChild(primitive);
            }

            doc.Save(saveFileDialog1.FileName);
        }

        private void импортXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(openFileDialog1.FileName);

            canvas1.Primitives.Clear();
            XmlNode root = doc.FirstChild;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                XmlNode child = root.ChildNodes[i];
                if (child.Name != "canvas")
                    continue;
                canvas1.Init(child);
                break;
            }
        }
    }
}
