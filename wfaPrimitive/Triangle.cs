﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml;

namespace wfaPrimitive
{
    public class Triangle : BasePrimitive
    {
        public Point p1 { get; private set; }
        public Point p2 { get; private set; }
        public Point p3 { get; private set; }

        public Point[] pointArr { get; private set; } = { };

        public Triangle(Point p1, Point p2, Point p3, Color fillColor)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            pointArr = new[] {p1, p2, p3};
            this.FillColor = fillColor;
        }

        public Color FillColor { get; set; }

        public Triangle()
        {
        }
        public override bool DetectCollision(Point p)
        {
            GraphicsPath grp = new GraphicsPath();

            grp.AddLine(p1,p2);
            grp.AddLine(p2,p3);
            grp.CloseFigure();

            Region reg = new Region(grp);
            return reg.IsVisible(p);
        }

        public override void move(int deltaX, int deltaY)
        {
            var point = p1;
            point.X += deltaX;
            point.Y += deltaY;
            p1 = point;
            
            point = p2;
            point.X += deltaX;
            point.Y += deltaY;
            p2 = point;
            
            point = p3;
            point.X += deltaX;
            point.Y += deltaY;
            p3 = point;
            
            pointArr = new[] {p1, p2, p3};
        }

        public override void Init(XmlNode node)
        {
            XmlAttribute x = node.Attributes["x1"];
            XmlAttribute y = node.Attributes["y1"];

            if (x != null && y != null)
            {
                p1 = new Point(int.Parse(x.Value), int.Parse(y.Value));
            }

            x = node.Attributes["x2"];
            y = node.Attributes["y2"];

            if (x != null && y != null)
            {
                p2 = new Point(int.Parse(x.Value), int.Parse(y.Value));
            }
            
            x = node.Attributes["x3"];
            y = node.Attributes["y3"];

            if (x != null && y != null)
            {
                p3 = new Point(int.Parse(x.Value), int.Parse(y.Value));
            }

            pointArr = new[] {p1, p2, p3};

            XmlAttribute attr = node.Attributes["color"];
            if (attr == null)
                return;

            switch (attr.Value)
            {
                case "red":
                    FillColor = Color.Red;
                    break;
                case "green":
                    FillColor = Color.Green;
                    break;
                case "blue":
                    FillColor = Color.Blue;
                    break;
            }
        }
    }
}