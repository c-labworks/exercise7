﻿using System.Drawing;
using System.Xml;

namespace wfaPrimitive
{
    public class Line : BasePrimitive
    {
        
        public Line() { }
        
        public Line(int x1, int y1, int x2, int y2)
        {
            left = x1;
            top = y1;
            width = x2;
            height = y2;
        }
        
        public int x1
        {
            get => left;
            private set => left = value;
        }

        public int y1
        {
            get => top;
            private set => top = value;
        }

        public int x2
        {
            get => width;
            private set => width = value;
        }

        public int y2
        {
            get => height;
            private set => height = value;
        }

        public override bool DetectCollision(Point p)
        {
            return (p.X - x1) * (y2 - y1) - (p.Y - y1) * (x2 - x1) == 0;
        }

        public override void move(int deltaX, int deltaY)
        {
            x1 += deltaX;
            x2 += deltaX;
            y1 += deltaY;
            y2 += deltaY;
        }

        public override void Init(XmlNode node)
        {
            XmlAttribute attr = node.Attributes["x1"];
            if (attr != null)
                left = int.Parse(attr.Value);

            attr = node.Attributes["y1"];
            if (attr != null)
                top = int.Parse(attr.Value);

            attr = node.Attributes["x2"];
            if (attr != null)
                width = int.Parse(attr.Value);

            attr = node.Attributes["y2"];
            if (attr != null)
                height = int.Parse(attr.Value);        
        }
    }
}