﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wfaPrimitive
{
    public class Ellipse : BasePrimitive
    {
        public Ellipse() { }
        public Ellipse(int left, int top, int width, int height)
        {
            this.left = left;
            this.top = top;
            this.width = width;
            this.height = height;
        }

        public override bool DetectCollision(Point p)
        {
            double x0 = left + width / 2.0;
            double y0 = top + height / 2.0;

            double a = width / 2;
            double b = height / 2;
            int x = p.X;
            int y = p.Y;

            double xa = (x - x0) / a;
            double yb = (y - y0) / b;

            double res = xa * xa + yb * yb;

            return res <= 1;
        }

        public override void move(int deltaX, int deltaY)
        {
            left += deltaX;
            top += deltaY;
        }
    }
}
