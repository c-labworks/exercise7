﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wfaPrimitive
{
    public interface IPrimitive
    {
        int left { get; set; }
        int top { get; set; }
        int width { get; set; }
        int height { get; set; }
    }
}
